# 第 7 小节：提交第一个 Pull Request

```
merge pull request的流程相对简单，是通过两个自己项目的repo进行的阐述，需要补充权限说明，code review，approve，CI机制等  
```

####  什么是 Pull Request

这个是由 GitHub 提出的概念。根据 GitHub 的官方文档，Pull Request 是一种通知机制，它可以告知仓库的其他开发者，你推送了一个仓库新分支。 通过Pull Request ，你可以和仓库的协作者一起讨论和审查提交的代码，审查通过后，就可以提交到仓库的主分支中。

Pull Request 本质上是一种协同工作的机制，可以进行基于网络的多人提交，审核机制，审核通过后，就可以合并到主分支中。

####  提交 Pull Request 的步骤

第一步，你需要 Fork 别人的仓库。也就是复制别人的仓库到你自己的仓库中。

第二步，在你自己的仓库上，修改然后提交之后，在网页端点击「New pull request」按钮。

在接下来的网页上有 base 仓库和 head 仓库，其中 base 是你想要提交的仓库，head 分支是你已经提交变更的仓库。

第三步，在提交中，描述提交本地提交的说明。

#### 实际操作一下

##### Gitee

1. 在 Gitee 上建立两个帐号 A 和 B。
![账号A](https://images.gitee.com/uploads/images/2021/0410/150351_aaf54390_8456984.png "账号A.png")
![账号B](https://images.gitee.com/uploads/images/2021/0410/150408_a80b686f_8456984.png "账号B.png")

2. 使用 A 帐号，新建仓库 pull_request_demo
![新建仓库1](https://images.gitee.com/uploads/images/2021/0410/150429_3f7cb023_8456984.png "新建仓库1.png")
![新建仓库2](https://images.gitee.com/uploads/images/2021/0410/150447_58b43a00_8456984.png "新建仓库2.png")
![新建仓库3](https://images.gitee.com/uploads/images/2021/0410/150501_421219a8_8456984.png "新建仓库3.png")

3. 在本地提交 README.md

   ```
   echo "pull_request_demo from A" >> README.md
   git init
   git add README.md
   git commit -m "first commit"
   git branch -M main
   git remote add origin https://gitee.com/A/pull_request_demo.git
   git push -u origin master
   ```

![本地提交](https://images.gitee.com/uploads/images/2021/0410/150531_69fb8ed9_8456984.gif "在本地提交 README.md.gif")

4. 使用 B 帐号登录 Gitee，然后 Fork 该项目。
![Fork项目1](https://images.gitee.com/uploads/images/2021/0410/150546_b6c50179_8456984.png "B账号fork项目1.png")
![Fork项目2](https://images.gitee.com/uploads/images/2021/0410/150559_4e6f6afc_8456984.png "B账号fork项目2.png")
![Fork项目3](https://images.gitee.com/uploads/images/2021/0410/150610_4351a9ea_8456984.png "B账号fork项目3.png")

5. 下载项目到本地

   ```
   git clone https://gitee.com/B/pull_request_demo
   echo "pull_request_demo add form B" >> README.md
   git add README.md
   git commit -m "modify commit"
   git push
   ```

![下载到本地](https://images.gitee.com/uploads/images/2021/0410/150637_0c8c1499_8456984.gif "下载项目到本地.gif")

6. 使用 B 帐号登录 Gitee，进入 pull_request_demo 仓库，点击 「+ Pull request」链接。
![Pull-1](https://images.gitee.com/uploads/images/2021/0410/150654_687717a7_8456984.png "pull_requests1.png")
![Pull-2](https://images.gitee.com/uploads/images/2021/0410/150708_fe12dee3_8456984.png "pull_requests2.png")

7. 选择 源分支 和 目标分支。
![源分支](https://images.gitee.com/uploads/images/2021/0410/150915_d55c2b70_8456984.png "源分支.png")
![目标分支](https://images.gitee.com/uploads/images/2021/0410/150928_84fd4489_8456984.png "目标分支.png")

8. 填写提交说明后，点击「创建」按钮。
![填写说明](https://images.gitee.com/uploads/images/2021/0410/150946_953a2d7e_8456984.png "pull_requests3.png")
![点击创建](https://images.gitee.com/uploads/images/2021/0410/155106_78a14137_8456984.png "点击创建.png")

9. 使用 A 帐号登录 Gitee，进入 pull_request_demo 项目。
![进入项目](https://images.gitee.com/uploads/images/2021/0410/160712_621d0351_8456984.png "进入项目.png")

10. 可以看到 Pull request 中有新的数据。
![有数据](https://images.gitee.com/uploads/images/2021/0410/160726_a311f8f8_8456984.png "有新数据.png")

11. 点击「合并」按钮，即可完成。
![合并](https://images.gitee.com/uploads/images/2021/0410/160740_eff52230_8456984.gif "合并.gif")

##### Github

1. 在 GitHub 上建立两个帐号 A 和 B。
![账号A](https://images.gitee.com/uploads/images/2021/0412/210726_a1f2fb20_8456984.png "账号A.png")
![账号B](https://images.gitee.com/uploads/images/2021/0412/210742_4cf5244b_8456984.png "账号B.png")

2. 使用 A 帐号，创建项目 pull_request_demo
![新建仓库](https://images.gitee.com/uploads/images/2021/0412/210813_1923ef71_8456984.gif "新建仓库.gif")

3. 在本地提交 README.md

   ```
   echo "pull_request_demo from A" >> README.md
   git init
   git add README.md
   git commit -m "first commit"
   git branch -M main
   git remote add origin https://github.com/A/pull_request_demo.git
   git push -u origin main
   ```
![本地提交](https://images.gitee.com/uploads/images/2021/0412/210834_1996cb81_8456984.gif "本地提交 README.md.gif")

4. 使用 B 帐号登录 GitHub，然后 Fork 该项目。
![Fork项目1](https://images.gitee.com/uploads/images/2021/0412/210900_ec78c0a3_8456984.png "B账号Fork项目1.png")
![Fork项目2](https://images.gitee.com/uploads/images/2021/0412/210912_81ab2e4b_8456984.png "B账号Fork项目2.png")

5. 下载项目到本地

   ```
   git clone https://github.com/B/pull_request_demo
   echo "pull_request_demo add form B" >> README.md
   git add README.md
   git commit -m "modify commit"
   git push
   ```
![下载项目到本地](https://images.gitee.com/uploads/images/2021/0412/211003_c2c14ec9_8456984.gif "下载项目到本地.gif")

6. 使用 B 帐号登录 GitHub，进入 pull_request_demo 仓库，点击 Pull request 链接。
![Pull项目1](https://images.gitee.com/uploads/images/2021/0412/211325_db7cf976_8456984.png "pull_request1.png")
![Pull项目2](https://images.gitee.com/uploads/images/2021/0412/211338_5f0754d0_8456984.png "pull_request2.png")

7. 选择 base 和 head 仓库。点击「Create pull request」按钮。
![选择Base仓库](https://images.gitee.com/uploads/images/2021/0412/211356_5be9f12f_8456984.png "base仓库.png")
![选择Head仓库](https://images.gitee.com/uploads/images/2021/0412/211415_eb747ed6_8456984.png "head仓库.png")
![点击Create pull request按钮](https://images.gitee.com/uploads/images/2021/0412/211618_853d205c_8456984.png "点击Create pull request按钮")

8. 填写提交说明后，点击「Create pull request」按钮。
![填写说明](https://images.gitee.com/uploads/images/2021/0412/211746_ebc51069_8456984.png "填写说明.png")
![提交](https://images.gitee.com/uploads/images/2021/0412/211802_7f52d86e_8456984.png "提交.png")

9. 使用 A 帐号登录 GitHub，进入 pull_request_demo 项目。
![进入项目1](https://images.gitee.com/uploads/images/2021/0412/211815_7a8e3dab_8456984.png "A账号进入项目1.png")
![进入项目2](https://images.gitee.com/uploads/images/2021/0412/211832_21255b22_8456984.png "A账号进入项目2.png")

10. 可以看到 Pull request 中有新的数据。
![有数据](https://images.gitee.com/uploads/images/2021/0412/211849_884cb0ca_8456984.png "有新数据.png")

11. 点击 Confirm merge，完成合并。
![合并](https://images.gitee.com/uploads/images/2021/0412/213111_8df051bf_8456984.gif "合并.gif")

#### 补充内容：参与公共的 PR 仓库来完成第一次PR尝试

当然，如果你不想注册两个 GitHub 账户亦或由于某些原因对此感到比较麻烦的话，你也可以按照上面的说明来选择参与到 GitHub 上的一些公共且活跃维护的 PR 尝试仓库。

比如说 [这个仓库](https://github.com/ituring/first-pr) （不过第一次提交时你只能作为提交者进行尝试）。

当你的 PR 被通过合并后，你的账户通常也会得到管理员权限，在此时你也可以来尝试作为管理员的身份来合并其他人的提交。

是的，在此时你的心中可能已经有了一个猜想，而我们在这里也可以告诉你这个猜想没有问题，像这些仓库其实还有很多，而且通常是由全体自发的贡献者来自愿进行维护的，而你也可以成为其中的一员。

### 本部分内容贡献者

[brace](https://gitee.com/awang)、[李志鹏](https://gitee.com/lizhipeng1992)、[ORH](https://gitee.com/orh)、[ShardingSphere](https://gitee.com/dangdangdotcom_zhangliang)、[冰彦糖](https://gitee.com/bingyantang)

> 发现内容中的错误？还是想要补充更多符合主题的内容？《开源指北》欢迎你进行贡献，点击[贡献指南](./../贡献指南.md)了解贡献的具体步骤。